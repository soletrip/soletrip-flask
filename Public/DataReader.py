'''
Created on 2015��7��15��

@author: shitian
'''

import Queue
import memcache

class DataReader(object):
    
    def __init__(self):
        pass
    
class DataBaseReader(object):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        self.__dbCache = memcache.Client('localhost')
        self.__dbClient = None
    
    def exeSQL(self,SQL):
        if SQL.lower().startswith('select'):
            result = self.__dbCache.get(SQL)
            if not result:
                result = self.__dbClient.exeSQL(SQL)
                self.__dbCache.set(SQL,result)
            return result
        else:
            return self.__dbCache.exeSQL(SQL)
    
class SQueue(Queue.Queue):
    
    def __init__(self,maxsize =-1):
        Queue.Queue.__init__(self, maxsize)
        
class Stack(Queue.LifoQueue):
    
    def __init__(self,maxsize =-1):
        Queue.LifoQueue.__init__(self, maxsize)
        
class BlockQueue(Queue.Queue):
    
    def __init__(self,maxsize =-1):
        Queue.Queue.__init__(self, maxsize)

    def get(self, timeout=None):
        return Queue.Queue.get(self, True, timeout=timeout)