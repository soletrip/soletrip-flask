# Set the path
import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from flask.ext.script import Manager, Server
from subsystem.systemloader import SystemLoader
if __name__ == "__main__":
    app = SystemLoader.initLoader()
    print app
    manager = Manager(app)
    # Turn on debugger by default and reloader
    manager.add_command("runserver", Server(use_debugger = True,use_reloader = True,host = 'localhost'))
    manager.run()