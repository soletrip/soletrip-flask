import unittest
from soletrip import app, db

class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_homepage(self):
        """
        Test home pages
        """
        rv = self.app.get('/')
        self.assertEqual(200, rv.status_code)
        self.assertEqual('Hello World!', rv.data)

if __name__ == '__main__':
    unittest.main()