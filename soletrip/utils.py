import pymongo

def initIds(name):
    c = pymongo.MongoClient()
    db = c.test
    db.ids.save({'_id': name, 'seq': 0})

def getId(name):
    c = pymongo.MongoClient()
    db = c.test
    id = []
    try:
        id = db.ids.find_and_modify(update={'$inc':{'seq': 1}}, query={'_id': name}, new=True)
        print id['seq']
    except TypeError as err:
        print err
        db.ids.save({'_id': name, 'seq': 0})
        id = db.ids.find_and_modify(update={'$inc':{'seq': 1}}, query={'_id': name}, new=True)
    finally:
        return id['seq']