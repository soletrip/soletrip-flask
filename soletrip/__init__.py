from flask import Flask
from flask.ext.mongoengine import MongoEngine
from modules.Plans.views import Plans

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {'DB': 'test'}
app.config['SECRET'] = {'thisisaserectkey'}
app.config['DEBUG'] = {'True'}

db = MongoEngine(app)
db.init_app(app)

# Add Plans into blueprint for routing
app.register_blueprint(Plans)

@app.route('/')
def hello_world():
    return 'Hello World!'

if __name__ == '__main__':
    app.run()