"""
    file: importdata.py

    purpose: This file need to be run before testing.
    It will insert serveral dummy basic data about countries, cities, hotels and scenics

    run by: python importdata.py or just run from project
"""
from modules.Base.models import *
import mongoengine

countries = [
    'China',
    'France',
    'UK',
    'Germany',
]

cities = [
    'Beijing',
    'Tianjin',
    'Shanghai',
    'Wuhan',
    'Nanjing',
    'Chengdu',
    'Shenzhen',
    'Paris',
    'London',
    'Berlin',
]

hotels = [
    'Hotel 1',
    'Hotel 2',
    'Hotel 3',
    'Hotel 4',
    'Hotel 5',
]

scenics = [
    'The Great Wall',
    'Salty Lake',
    'Shanhai Guan',
    'San Xia',
    'Tower',
]

def generate_cities():
    i = 0
    for c in cities:
        city = B_City()
        city.name = c
        city.country = countries[i%4]
        city.save()
        i = i + 1

def generate_hotels():
    i = 0
    for h in hotels:
        hotel = B_Hotel()
        hotel.name = h
        hotel.address = h + ' address ' + str(i)
        hotel.save()
        i = i + 1

def generate_scenics():
    for s in scenics:
        scenic = B_Scenic()
        scenic.name = s
        scenic.save()

if __name__ == '__main__':
    mongoengine.connect('test')
    B_City.drop_collection()
    B_Hotel.drop_collection()
    B_Scenic.drop_collection()
    generate_cities()
    generate_hotels()
    generate_scenics()