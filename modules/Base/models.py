"""
    package: Base
"""
from mongoengine import *

class B_City(Document):
    name = StringField()
    country = StringField()

class B_Hotel(Document):
    name = StringField()
    address = StringField()

class B_Scenic(Document):
    name = StringField()
