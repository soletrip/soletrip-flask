import unittest
import json
from soletrip import app
from modules.Plans.models import Plan, DayPlan, City, Hotel, Journey
from modules.Base.models import *

cities = [
    'Beijing',
    'Tianjin',
    'Shanghai',
    'Wuhan',
    'Nanjing',
    'Chengdu',
    'Shenzhen',
    'Paris',
    'London',
    'Berlin',
]

class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def tearDown(self):
        pass

    def generate_plan(self):
        """
        Generate a plan for test
        """
        cities = []
        queryset = B_City.objects.all()
        for c in queryset:
            city = City()
            city.city_id = c
            city.name = c.name
            city.code = '0'
            cities.append(city)

        journeys = []
        queryset = B_Scenic.objects.all()
        for s in queryset:
            journey = Journey()
            journey.scenic_id = s
            journey.name = c.name
            journeys.append(journey)

        hotels = []
        queryset = B_Hotel.objects.all()
        for h in queryset:
            hotel = Hotel()
            hotel.hotel_id = h
            hotel.name = c.name
            hotels.append(hotel)

        day_plans = []
        day_plan = DayPlan()
        day_plan.cities = cities
        day_plan.journeys = journeys
        day_plan.accommodations = hotels
        day_plan.play_hours = 5
        day_plan.day_of_tour = 1
        day_plans.append(day_plan)
        day_plan.day_of_tour = 2
        day_plans.append(day_plan)
        day_plan.day_of_tour = 3
        day_plans.append(day_plan)
        day_plan.day_of_tour = 4
        day_plans.append(day_plan)

        plan = Plan(day_plans=day_plans)
        # plan.cities = ['chengdu', 'paris', 'london', 'berlin']
        plan.days = 4
        plan.save()
        return plan.id

    def test_empty_plans(self):
        """
        Test get empty Plans list
        """
        Plan.drop_collection()
        rv = self.app.get('/plans/')
        self.assertEqual(200, rv.status_code)
        list = json.loads(rv.data)
        self.assertEqual(0, len(list))

    def test_plans_get(self):
        """
        Test get and add Plans API
        """
        Plan.drop_collection()
        p_id = self.generate_plan()
        p_id = self.generate_plan()
        rv = self.app.get('/plans/')
        self.assertEqual(200, rv.status_code)
        list = json.loads(rv.data)
        self.assertEqual(2, len(list))
        for i in list:
            plan = Plan.from_json(json.dumps(i))
            self.assertEqual(4, plan.days)
            self.assertEqual(4, len(plan.day_plans))
            day_plans = plan.day_plans
            for day_plan in day_plans:
                self.assertEqual(10, len(day_plan.cities))
                j = 0
                for city in day_plan.cities:
                    self.assertEqual(cities[j], city.name)
                    j = j + 1
                self.assertEqual(5, len(day_plan.journeys))

    def test_plans_post(self):
        """
        Test add Plans API
        """
        Plan.drop_collection()
        cities = []
        queryset = B_City.objects.all()
        for c in queryset:
            city = City()
            city.city_id = c
            city.name = c.name
            city.code = '0'
            cities.append(city)

        journeys = []
        queryset = B_Scenic.objects.all()
        for s in queryset:
            journey = Journey()
            journey.scenic_id = s
            journey.name = c.name
            journeys.append(journey)

        hotels = []
        queryset = B_Hotel.objects.all()
        for h in queryset:
            hotel = Hotel()
            hotel.hotel_id = h
            hotel.name = c.name
            hotels.append(hotel)

        day_plans = []
        day_plan = DayPlan()
        day_plan.cities = cities
        day_plan.journeys = journeys
        day_plan.accommodations = hotels
        day_plan.play_hours = 5
        day_plan.day_of_tour = 1
        day_plans.append(day_plan)
        day_plan.day_of_tour = 2
        day_plans.append(day_plan)
        day_plan.day_of_tour = 3
        day_plans.append(day_plan)
        day_plan.day_of_tour = 4
        day_plans.append(day_plan)

        plan = Plan(day_plans=day_plans)
        # plan.cities = ['chengdu', 'paris', 'london', 'berlin']
        plan.days = 4
        rv = self.app.post('/plans/', data=plan.to_json())
        self.assertEqual(201, rv.status_code)
        rp = Plan.from_json(rv.data)
        self.assertEqual(4, rp.days)
        self.assertEqual(4, len(plan.day_plans))
        day_plans = rp.day_plans
        for day_plan in day_plans:
            self.assertEqual(10, len(day_plan.cities))
            j = 0
            for city in day_plan.cities:
                j = j + 1
            self.assertEqual(5, len(day_plan.journeys))


    def test_plan_get_delete(self):
        """
        Test get Plan API
        """
        Plan.drop_collection()
        p_id = self.generate_plan()
        print type(p_id), p_id
        rv = self.app.get('/plans/' + str(p_id) + '/')
        self.assertEqual(200, rv.status_code)
        plan = Plan.from_json(json.dumps(json.loads(rv.data)[0]))
        self.assertEqual(p_id, plan.id)

        rv = self.app.delete('/plans/' + str(p_id) + '/')
        self.assertEqual(201, rv.status_code)
        self.assertEqual(str(p_id), rv.data)

    def test_plan_update(self):
        """
        Test update and get Plan API
        """
        Plan.drop_collection()
        p_id = self.generate_plan()
        rv = self.app.get('/plans/' + str(p_id) + '/')
        self.assertEqual(200, rv.status_code)
        plan = Plan.from_json(json.dumps(json.loads(rv.data)[0]))
        self.assertEqual(p_id, plan.id)

        plan.day_plans = plan.day_plans[:1]
        plan.days = 1
        rv = self.app.put('/plans/' + str(p_id) + '/', data=plan.to_json())
        self.assertEqual(201, rv.status_code)

        rv = self.app.get('/plans/' + str(p_id) + '/')
        self.assertEqual(200, rv.status_code)
        plan = Plan.from_json(json.dumps(json.loads(rv.data)[0]))
        self.assertEqual(p_id, plan.id)
        self.assertEqual(1, plan.days)

if __name__ == '__main__':
    unittest.main()
