from mongoengine import *

import datetime

class Hotel(EmbeddedDocument):
    hotel_id = ReferenceField('B_Hotel')
    name = StringField(required=True, max_length=32)

class City(EmbeddedDocument):
    city_id = ReferenceField('B_City')
    name = StringField(required=True, max_length=64)
    code = StringField(max_length=16)

class Journey(EmbeddedDocument):
    scenic_id = ReferenceField('B_Scenic')
    name = StringField(required=True, max_length=64)
    period = IntField()
    amount = FloatField()

class DayPlan(EmbeddedDocument):
    cities = EmbeddedDocumentListField(City)
    journeys = EmbeddedDocumentListField(Journey)
    accommodations = EmbeddedDocumentListField(Hotel)
    # traffics = ListField(ReferenceField(Traffic)) Need basic data
    play_hours = IntField() # hours of touring of a day
    day_of_tour = IntField() # the day of tour, 1 means day 1 of tour

class Plan(Document):
    day_plans = EmbeddedDocumentListField(DayPlan)
    days = IntField()
    created_at = DateTimeField(default=datetime.datetime.now())