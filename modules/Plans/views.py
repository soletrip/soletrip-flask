from flask import request, Blueprint
from models import *
from bson import ObjectId

Plans = Blueprint('plans', __name__)

@Plans.route('/plans/', methods=['GET', 'POST'])
def plans():
    """
    Get or Post Plan via /plans
    :return:
    * 200 for get plans list
    * 201 for post new plan
    """
    if request.method == 'GET':
        plans = Plan.objects.all()
        return plans.to_json()
    elif request.method == 'POST':
        print type(request.data), request.data
        plan = Plan.from_json(request.data)
        plan.save()
        return plan.to_json(), 201

@Plans.route('/plans/<plan_id>/', methods=['GET', 'POST', 'PUT', 'DELETE'])
def plan(plan_id):
    """
    Handling CRUD on Plan
    :param id: id of plan in document
    :return:
    200 for get plan detail
    201 for create, update or delete succcessfully
    """
    if request.method == 'GET':
        plan = Plan.objects(id=ObjectId(plan_id))
        return plan.to_json()
    if request.method == 'POST' or request.method == 'PUT':
        print request.data
        new_plan = Plan.from_json(request.data)
        Plan.objects(id=new_plan.id).update_one(set__day_plans=new_plan.day_plans
                                                , set__days=new_plan.days)
        return str(new_plan.id), 201
    if request.method == 'DELETE':
        plan = Plan.objects(id=ObjectId(plan_id))
        plan.delete()
        return plan_id, 201