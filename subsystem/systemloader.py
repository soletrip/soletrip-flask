import threading
import os,json,time,sys
from flask import Flask
from flask import Blueprint
from flask.ext.mongoengine import MongoEngine

class SubSystemRunner(threading.Thread):
    def __init__(self):
        self.__runnerInfo = None
        threading.Thread.__init__(self)
        
    def run(self):
        threading.Thread.run(self)
        if not self.__runnerInfo:
            return
        sloader = self.__runnerInfo.get('url')
        spath = sloader['rander_pkg']
        pathx = os.getcwd()
        for fpath in spath.split('.'):
            pathx = os.path.join(pathx,fpath)
        sys.path.append(pathx)
        subsys = __import__(sloader['rander_py'])
        reload(subsys)
        subsysCls = getattr(subsys, sloader['rander_cls'])
        ssc  = subsysCls()
        ssc.run(sloader.get('paras'))
        
    def startRunner(self,runnerInfo):
        self.__runnerInfo = runnerInfo
        self.start()
        
class SubSystemMgr(object):
    def __init__(self):
        self.__runnerList = {}
        
    def initRunner(self,configs):
        subSystem = SubSystemRunner()
        keystr = self.generateDKey(configs)
        if self.__runnerList.get(keystr):
            self.__runnerList[keystr] = subSystem
            subSystem.startRunner(configs)
            
    def generateDKey(self,dictObject):
        string = ""
        string += ('.' + dictObject.get('rander_pkg'))
        string += ('.' + dictObject.get('rander_cls'))
        string += ('.' + dictObject.get('rander_py'))
        paras = {}
        keys = paras.keys()
        values = paras.values()
        keyToString = '.'.join(keys)
        valueToString = '.'.join(values)
        string += keyToString
        string += valueToString
        return string
        
class SystemLoader(threading.Thread):
    __systemLoader = None
    subSystemFolder = os.path.join(os.getcwd(),'subsystem')
    subSystemDict = {}
    subSystemManager = None
    systemAPP = None
    systemBlueSprint = None
    
    def __init__(self, group=None, target=None, name=None, 
        args=(), kwargs=None, verbose=None):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=verbose)
    
    @staticmethod
    def getLoader():
        if not SystemLoader.__systemLoader:
            SystemLoader.__systemLoader = SystemLoader()
            SystemLoader.subSystemManager = SubSystemMgr()
        return SystemLoader.__systemLoader
    
    @staticmethod
    def initLoader():
        if not SystemLoader.__systemLoader:
            SystemLoader.__systemLoader = SystemLoader()
            SystemLoader.__systemLoader.loadAPP()
        SystemLoader.__systemLoader.start()
        return SystemLoader.__systemLoader.systemAPP
        
    def handleUrlConfig(self,subConfigJson=None):
        if not subConfigJson:
            return
        if subConfigJson.get('url').lower() == 'system':
            self.subSystemManager.initRunner(subConfigJson)
        else:
            if self.subSystemDict.has_key(subConfigJson['url']):
                return
            self.subSystemDict[subConfigJson['url']] = subConfigJson 
            self.registerSubSystem(subConfigJson['url'])
    
    def run(self):
        threading.Thread.run(self)
        while True:
            for subsys in os.listdir(self.subSystemFolder):
                configJson = os.path.join(self.subSystemFolder,subsys,'config.json')
                if not os.path.exists(configJson):
                    continue
                f = open(configJson)
                configdict = json.load(f)
                f.close()
                if configdict.get('url'):
                    self.handleUrlConfig(configdict)
                if configdict.get('systems'):
                    for sysItem in configdict.get('systems'):
                        self.handleUrlConfig(sysItem)
            print 'sub sytem : ',[self.subSystemDict]
            print 'sub sytems : ',[self.subSystemDict]
            time.sleep(60)
            
    def registerSubSystem(self,url):
        print "registerSubSystem",self.subSystemDict,self.subSystemDict.keys(),url
        url = url.rstrip('/').rstrip('\\')
        if url in self.subSystemDict.keys():
            sloader = self.subSystemDict.get(url)
            print sloader,sloader.has_key('registered')
            if sloader.has_key('registered'):
                return
            spath = sloader['rander_pkg']
            pathx = os.getcwd()
            for fpath in spath.split('.'):
                pathx = os.path.join(pathx,fpath)
            sys.path.append(pathx)
            subsys = __import__(sloader['rander_py'])
            reload(subsys)
            subsysCls = getattr(subsys, sloader['rander_cls'])
            ssc  = subsysCls()
            print url,None,ssc.index
            self.systemAPP.add_url_rule('/'+url,None,ssc.index,methods=['GET', 'POST', 'PUT', 'DELETE'])
            self.subSystemDict[url]['registered'] = True
            print self.systemAPP.url_map
            
    def getSubSystemResponse(self,req,url):
        response = None
        print "getSubSystemResponse",self.subSystemDict,self.subSystemDict.keys(),url
        url = url.rstrip('/').rstrip('\\')
        if url in self.subSystemDict.keys():
            sloader = self.subSystemDict.get(url)
            spath = sloader['rander_pkg']
            pathx = os.getcwd()
            for fpath in spath.split('.'):
                pathx = os.path.join(pathx,fpath)
            sys.path.append(pathx)
            subsys = __import__(sloader['rander_py'])
            reload(subsys)
            subsysCls = getattr(subsys, sloader['rander_cls'])
            ssc  = subsysCls()
            response = ssc.index(req)
        if response:
            return response
        else:
            pass#return HttpResponse('Sub sytem not found!')

    def loadAPP(self):
        self.systemAPP = Flask(__name__)
        self.systemAPP.config['MONGODB_SETTINGS'] = {'DB': 'test'}
        self.systemAPP.config['SECRET'] = {'thisisaserectkey'}
        self.systemAPP.config['DEBUG'] = {'True'}
        db = MongoEngine(self.systemAPP)
        db.init_app(self.systemAPP)
        self.systemBlueSprint = Blueprint('soletrip',__name__)
