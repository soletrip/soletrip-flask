from django.http.response import HttpResponse
from django.shortcuts import render_to_response
import os
from django.template.base import Template
from django.template.context import Context
from django import forms

# Create your views here.
class UserAccount(forms.Form):
    emailAddress = forms.EmailField(label='Username',max_length=30)
    password = forms.CharField(label='Password',widget=forms.PasswordInput)
    
class Logon(object):
    
    def logont(self,req):
        print "This is logon html test! "
        #html = open(os.path.join('templates','logon.html'))
        #strhtml = html.read()
        #t = Template(unicode(strhtml))
        t = Template("<h1>This is a test html{{x}}</h1>")
        print "html done"
        #html.close()
        rc = Context({'x':'.'})
        t = Template('My name is {{ name }}.')
        c = Context()
        r = HttpResponse(t.render(c))
        return r
    
    def logon(self,req):
        return render_to_response('logon.html')

    def login(self,req):
        if req.method == 'POST':
            form = UserAccount(req.POST)
            if form.is_valid():
                print form.cleaned_data
                return HttpResponse(str(form.cleaned_data))
        else:
            form = UserAccount()
        print os.getcwd()
        return render_to_response('logon.html',{'form':form,'imgserver':"http://116.255.233.233:8088"})
    
    def index(self):
        print "Views.index"
        return "This is a test string"
        #return self.login(req)