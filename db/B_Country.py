# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from mongoengine.fields import ComplexDateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Country(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)  ### '中文名称',
    en_name = StringField(max_length = 45)  ### '英文名称',
    city_count = IntField()  ### '国家拥有多少城市',
    hot = IntField()  ### '热门国家',
    last_update = ComplexDateTimeField()
    #continent_id = ReferenceField(B_Continent)
    continent_cn_name = StringField(max_length = 45)  ### '国家所在洲的中文名称',
    continent_en_name = StringField(max_length = 45)  ### '国家所在洲的英文名称', 
    ### '国家基础信息表';
