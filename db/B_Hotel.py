# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_3pty_Company import B_3pty_Company
from subsystem.db.B_City import B_City
from mongoengine.fields import ReferenceField, ListField
from mongoengine.fields import FloatField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Hotel(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)
    en_name = StringField(max_length = 45)
    longitude = FloatField()  ### '经度',
    latitude = FloatField()  ### '纬度',
    address = StringField(max_length = 45)  ### '地址',
    cn_introduction = StringField(max_length = 45)  ### '酒店介绍（中文）',
    en_introduction = StringField(max_length = 45)  ### '酒店介绍（英文）',
    phonenum = StringField(max_length = 45)  ### '电话1',
    website = StringField(max_length = 45)  ### '酒店主页',
    hot = IntField()  ### '是否人们酒店',
    grade = IntField()  ### '星级\n（1）经济，\n（2）3星，\n（3）4星，\n（4）5星',
    grade_desc = StringField(max_length = 45)  ### '星级描述，用于内部编辑理解',
    device = IntField()  ### '酒店设备提供\n（1）wifi   0x01\n（2）暖气 0x02\n（3）空调 0x04\n（4）……\n多选项，依赖于数据库位操作\n',
    device_desc = StringField(max_length = 45)
    area = IntField()  ### '所在区域\n（1）市区以内\n（2）郊区周边\n（3）……',
    area_desc = StringField(max_length = 45)  ### '区域说明',
    city_id = ReferenceField(B_City)
    company_id = ReferenceField(B_3pty_Company)
    type_id = ListField() ### type id defined in xml.
    ### '（1）酒店基础信息表\n（2）依赖于酒店API来去人价格';
