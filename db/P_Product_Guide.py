# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.P_Journey_City import P_Journey_City
from subsystem.db.B_Guide import B_Guide
from subsystem.db.P_Product_Info import P_Product_Info
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField


class P_Product_Guide(Document):
    guide_type = StringField(max_length = 45)
    note = StringField(max_length = 45)
    product_info_id = ReferenceField(P_Product_Info)
    guide_id = ReferenceField(B_Guide)
    journey_city_id = ReferenceField(P_Journey_City)
    ### '产品导游信息';
