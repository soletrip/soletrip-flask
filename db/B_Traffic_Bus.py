# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_Traffic import B_Traffic
from subsystem.db.B_Guide import B_Guide
from subsystem.db.B_Country import B_Country
from subsystem.db.B_City import B_City
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Traffic_Bus(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)
    en_name = StringField(max_length = 45)
    type = IntField()  ### '车辆类型\n(1)5座以下\n(2)8～9座\n(3)15～27座\n(4)29～33座',
    grade = IntField()  ### '旅游车等级',
    grade_desc = StringField(max_length = 45)  ### '旅游车等级说明',
    price = StringField(max_length = 45)
    city_id = ReferenceField(B_City)
    country_id = ReferenceField(B_Country)
    guide_id = ReferenceField(B_Guide)
    traffic_id = ReferenceField(B_Traffic)
    money_type_id = IntField() ### index in money type defined xml
    ### '旅游大巴车基础信息表,对于B_Traffic表中bus的补充';
