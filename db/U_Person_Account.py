# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.U_Company_Account import U_Company_Account
from mongoengine.fields import ReferenceField
from mongoengine.fields import DateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField
from subsystem.db.F_Sub_Folder import F_Sub_Folder


class U_Person_Account(Document):
    id = ObjectIdField(required=True,primary_key=True)
    account = StringField(required=True,max_length = 45)
    password = StringField(required=True,max_length = 45)
    nick_name = StringField(max_length = 45)
    birthday = DateTimeField()
    sex = StringField(max_length = 45)
    phone_number = StringField(max_length = 45)
    qq = StringField(max_length = 45)
    wechat = StringField(max_length = 45)
    weibo = StringField(max_length = 45)
    disable = IntField(required=True)  ### '（1）是否被公司帐号设置为disable，比如员工离职时\n（2）只能由公司帐号设定该值',
    company_account_id = ReferenceField(U_Company_Account)
    sub_folder_id = ReferenceField(F_Sub_Folder)
