# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.P_Journey_Info import P_Journey_Info
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField
from mongoengine.fields import ComplexDateTimeField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class P_Product_Info(Document):
    id = ObjectIdField(required=True,primary_key=True)
    journey_start_time = ComplexDateTimeField()  ### '产品开始时间',
    journey_end_timet = ComplexDateTimeField()  ### '产品结束时间',
    travel_start_time = ComplexDateTimeField()  ### '游玩开始时间',
    travel_end_time = ComplexDateTimeField()  ### '游玩结束时间',
    man_num = IntField()  ### '产品人数',
    adult_num = StringField(max_length = 45)  ### '成人数',
    child_num = StringField(max_length = 45)  ### '小孩',
    elder_num = StringField(max_length = 45)  ### '老人个数',
    use_car = IntField()
    use_guide = IntField()
    note = StringField(max_length = 45)
    journey_info_id = ReferenceField(P_Journey_Info)
