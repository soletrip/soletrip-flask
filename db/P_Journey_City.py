# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.P_Journey_Info import P_Journey_Info
from mongoengine.fields import ReferenceField
from mongoengine.fields import FloatField
from mongoengine.fields import DateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class P_Journey_City(Document):
    id = ObjectIdField(required=True,primary_key=True)
    prev_city_id = IntField()
    cur_city_id = IntField(required=True)
    note = StringField(max_length = 45)  ### '该城市行程简述',
    overnight = IntField()  ### '在该城市过夜',
    start_journey_day = IntField()  ### '城市游玩开始时间在行程的第几天',
    start_journey_time = DateTimeField()  ### '城市游玩开始时间在当天的几点几分',
    end_journey_day = IntField()  ### '城市游玩结束时间在旅程的第几天',
    end_journey_time = DateTimeField()  ### '城市游玩结束时间在当天的几点',
    total_day = FloatField()  ### '在该城市停留时间',
    journey_Info_id = ReferenceField(P_Journey_Info)
    prev_journey_id = ObjectIdField()
    ### '整个行程中旅游的城市';
