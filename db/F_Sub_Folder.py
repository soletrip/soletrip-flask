# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.F_Root_Folder import F_Root_Folder
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class F_Sub_Folder(Document):
    id = ObjectIdField(required=True,primary_key=True)
    level = IntField()  ### '（1）该folder处于哪一层子folder\n（2）设定最大folder level',
    name = StringField(required=True,max_length = 45)
    type = IntField(required=True)  ### '文件夹类型\n(1)Personal Folder\n(2)Group Folder\n(3)Share Folder',
    sub_type = IntField()  ### '对于Personal Folder\n(1)Private \n(2)Share\n   \n对于Group Folder\n\n对于Share Folder\n(1)\n(2)',
    root_folder_id = ReferenceField(F_Root_Folder)
    parent_folder_id = ObjectIdField()#ReferenceField(F_Sub_Folder)
