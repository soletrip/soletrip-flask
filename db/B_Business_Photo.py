# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Business_Photo(Document):
    id = ObjectIdField(required=True,primary_key=True)
    basic_data_id = IntField()  ### '每一种基础数据在表中的id',
    photo_url = StringField()
    note = StringField(max_length = 45)
    business_type_id = IntField()
    ### '所有图片的基础数据库\n只存储图片的url\n依赖于id＋business_type_id做到唯一性';
