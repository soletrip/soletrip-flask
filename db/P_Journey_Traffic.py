# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.P_Journey_City import P_Journey_City
from subsystem.db.B_Traffic_City import B_Traffic_City
from subsystem.db.B_Traffic import B_Traffic
from mongoengine.fields import ReferenceField
from mongoengine.fields import FloatField
from mongoengine.fields import DateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class P_Journey_Traffic(Document):
    id = ObjectIdField(required=True,primary_key=True)
    distance = StringField(max_length = 45)  ### '两个城市之间的距离',
    traffic_type = StringField(max_length = 45)  ### '交通方式',
    note = StringField(max_length = 45)  ### '该段交通说明',
    start_journey_day = IntField(required=True)  ### '出发时间在行程的第几天',
    start_journey_time = DateTimeField(required=True)  ### '出发时间在当天的几点几分',
    end_journey_day = IntField(required=True)  ### '结束时间在旅程的第几天',
    end_journey_time = DateTimeField(required=True)  ### '结束时间在当天的几点',
    total_day = FloatField(required=True)
    traffic_id = ReferenceField(B_Traffic)
    traffic_city_id = ReferenceField(B_Traffic_City)
    journey_city_id = ReferenceField(P_Journey_City)
    ### '两个城市之间的交通可能需要两种交通工具，所以单独列表，关联到“城市表”上';
