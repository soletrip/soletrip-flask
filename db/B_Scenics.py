# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_City import B_City
from mongoengine.fields import ReferenceField
from mongoengine.fields import FloatField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Scenics(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)  ### '中文名字',
    en_name = StringField(max_length = 45)  ### '英文名字',
    first_name = StringField(max_length = 45)
    second_name = StringField(max_length = 45)
    local_name = StringField(max_length = 45)
    cn_overview = StringField(max_length = 45)  ### '景点概要介绍（中文）',
    en_overview = StringField(max_length = 45)  ### '景点概要介绍（英文）',
    longitude = FloatField()  ### '经度',
    latitude = FloatField()  ### '纬度',
    address = StringField(max_length = 45)  ### '地址',
    cn_introduction = StringField(max_length = 45)  ### '景点介绍',
    en_introduction = StringField(max_length = 45)
    business_hours = StringField(max_length = 45)  ### '营业时间介绍（中文）',
    notice = StringField(max_length = 45)  ### '注意事项',
    phonenum = StringField(max_length = 45)  ### '电话1',
    website = StringField(max_length = 45)
    grade_index = IntField()  ### '景点等级',
    cn_grade_desc = StringField(max_length = 45)  ### '景点等级描述（中文）',
    en_grade_desc = StringField(max_length = 45)  ### '景点等级描述（英文）',
    hot = IntField()  ### '是否为热门景点',
    spot_type = IntField()  ### '景点归类，比如\n（1）人文，\n（2）自然\n（3）……\n多选项，依赖于位操作',
    city_id = ReferenceField(B_City)
    ### '（1）景点基础信息表\n（2）依赖于人工审核';
    ticket_detail_info = StringField(max_length = 100)  ### '门票说明\n',
    ticket_price = IntField()  ### '价格',
    money_type_id = IntField() ### record the money type index.
    ### '（1）景点门票基础信息表\n（2）依赖于人工录入';
