# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_3pty_Company import B_3pty_Company
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField
from mongoengine.fields import ObjectIdField
from subsystem.db.F_Sub_Folder import F_Sub_Folder


class U_Company_Account(Document):
    id = ObjectIdField(required=True,primary_key=True)
    type = StringField(max_length = 45)  ### '账户业务范围\n（1）组团端\n（2）地接端\n（3）……',
    account = StringField(required=True,max_length = 45)
    password = StringField(required=True,max_length = 45)
    status = StringField(max_length = 45)  ### '帐号状态\n（1）注册验证中\n（2）login\n（3）loginout\n（4）……',
    third_party_company_id = ReferenceField(B_3pty_Company)
    root_folder_id = ReferenceField(F_Sub_Folder)
