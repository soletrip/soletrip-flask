# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_Country import B_Country
from mongoengine.fields import ReferenceField, IntField
from mongoengine.fields import StringField
from mongoengine.fields import ObjectIdField


class B_3pty_Company(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)
    en_name = StringField(max_length = 45)
    introduction = StringField(max_length = 45)  ### '公司介绍',
    website = StringField(max_length = 45)
    first_phone_num = StringField(max_length = 45)
    second_phone_num = StringField(max_length = 45)
    cert_id = StringField(max_length = 45)  ### '公司资质号',
    email = StringField(max_length = 45)
    country_id = ReferenceField(B_Country)
    business_type_id = IntField() ### business define in xml , only record id here 
    ### '所有旅游行业关联公司';
