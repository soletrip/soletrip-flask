# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_3pty_Company import B_3pty_Company
from subsystem.db.B_City import B_City
from mongoengine.fields import ReferenceField
from mongoengine.fields import DateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Traffic(Document):
    id = ObjectIdField(required=True,primary_key=True)
    number = StringField(max_length = 45)  ### '班次',
    cn_desc = StringField(max_length = 45)  ### '交通工具以及线路描述（中文）',
    en_desc = StringField(max_length = 45)  ### '交通工具以及线路描述（英文）',
    start_time = DateTimeField()
    end_time = DateTimeField()
    total_time = IntField()
    main_type = StringField(max_length = 45)  ### '交通工具类型\n（1）飞机\n（2）火车\n（3）邮轮\n（4）大巴\n（5）出租\n（6）租车',
    sub_type = StringField(max_length = 45)  ### '对各种交通工具的详细分类',
    start_city_id = ReferenceField(B_City)
    end_city_id = ReferenceField(B_City)
    company_id = ReferenceField(B_3pty_Company)
    ### '交通工具基础信息表\n（1）往返基础信息如何设计？\n（2）中转基础信息如何设计？';
