# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_Hotel import B_Hotel
from subsystem.db.P_Journey_City import P_Journey_City
from subsystem.db.P_Product_Info import P_Product_Info
from mongoengine.fields import ReferenceField
from mongoengine.fields import FloatField
from mongoengine.fields import ComplexDateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class P_Product_Hotel(Document):
    id = ObjectIdField(required=True,primary_key=True)
    hotel_type = StringField(max_length = 45)  ### '房型',
    number = IntField()  ### '房间数量',
    note = StringField(max_length = 45)  ### '入住说明',
    arrive_time = ComplexDateTimeField()  ### '到达城市时间\n',
    leave_time = ComplexDateTimeField()  ### '离开城市时间',
    total_time = FloatField()  ### '停留时间，按天数算，1小时就是0.1day',
    product_info_id = ReferenceField(P_Product_Info)
    journey_city_id = ReferenceField(P_Journey_City)
    hotel_id = ReferenceField(B_Hotel)
    ### '产品酒店信息';
