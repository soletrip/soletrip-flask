# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField
from mongoengine.fields import FloatField
from mongoengine.fields import DateTimeField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField
from subsystem.db.B_Traffic import B_Traffic
from subsystem.db.B_Country import B_Country


class B_Traffic_City(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cur_city_id = IntField(required=True)  ### '当前站所在城市',
    prev_city_id = IntField()  ### '前一个城市',
    next_city_id = IntField()  ### '后一个城市',
    traffic_id = IntField(required=True)  ### '属于B_Flight, B_Ship, B_Train等的id',
    arrive_time = DateTimeField()  ### '到达时间',
    leave_time = DateTimeField()  ### '离开时间',
    total_time = FloatField()  ### '按天数算，如果1小时就是0.1 day',
    note = StringField(max_length = 45)
    price = IntField()
    arrive_all_cities = IntField()  ### '整个国家的城市都可以到达\n(不需要在B_Traffic_City中再建立所要到达的城市)',
    arrive_cities_num = StringField(max_length = 45)  ### '可到达城市个数',
    traffic_id = ReferenceField(B_Traffic)
    country_id = ReferenceField(B_Country)
    #traffic_country_id = ReferenceField(B_Traffic_Country)
    #money_type_id = ReferenceField(B_Money_Type)
    money_type_id = IntField() ### index in money type defined xml
    ### '飞机／轮船／火车 \n到达每一个城市的时间';
