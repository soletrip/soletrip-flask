# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from mongoengine.fields import ObjectIdField


class O_Tourist_Info(Document):
    id = ObjectIdField(required=True,primary_key=True)
    ### '游客信息表';
