# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_City import B_City
from subsystem.db.B_3pty_Company import B_3pty_Company
from subsystem.db.B_Country import B_Country
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Guide(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)
    en_name = StringField(max_length = 45)
    cert_id = StringField(max_length = 45)  ### '导游证',
    price = StringField(max_length = 45)
    grade = IntField()  ### '导游等级',
    grade_desc = StringField(max_length = 45)  ### '导游等级说明',
    type = IntField()  ### '导游分类\n比如\n（1）中文 0x01\n（2）英文 0x02\n（3）法语 0x04\n（4）……',
    country_id = ReferenceField(B_Country)
    company_id = ReferenceField(B_3pty_Company)
    city_id = ReferenceField(B_City)
    type_id = IntField() ### type define in xml , record the id here.
    ### '导游基础信息表';
