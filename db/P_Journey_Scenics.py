# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_Scenics import B_Scenics
from subsystem.db.P_Journey_City import P_Journey_City
from mongoengine.fields import ReferenceField
from mongoengine.fields import StringField
from mongoengine.fields import FloatField
from mongoengine.fields import DateTimeField
from mongoengine.fields import ObjectIdField


class P_Journey_Scenics(Document):
    id = ObjectIdField(required=True,primary_key=True)
    arrive_time = DateTimeField()  ### '到达景点时间',
    leave_time = DateTimeField()  ### '离开景点时间',
    total_time = FloatField()  ### '默认以天来计量，小于1天，按小时计量',
    note = StringField(max_length = 45)
    journey_city_id = ReferenceField(P_Journey_City)
    cur_scenics_id = ReferenceField(B_Scenics)
    prev_scenics_id = ObjectIdField()
