# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_Country import B_Country
from mongoengine.fields import ReferenceField
from mongoengine.fields import FloatField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_City(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)
    en_name = StringField(max_length = 45)
    first_name = StringField(max_length = 45)
    second_name = StringField(max_length = 45)
    local_name = StringField(max_length = 45)
    longitude = FloatField()  ### '经度',
    latitude = FloatField()  ### '纬度',
    hot = IntField()  ### '热门城市',
    cn_overview = StringField(max_length = 45)  ### '城市概要介绍（中文）',
    en_overview = StringField(max_length = 45)  ### '城市概要介绍（英文）',
    country_id = ReferenceField(B_Country)
    ### '城市基础信息表';
