# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_Traffic import B_Traffic
from subsystem.db.P_Product_Info import P_Product_Info
from mongoengine.fields import ReferenceField
from mongoengine.fields import ComplexDateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import ObjectIdField


class P_Product_Traffic(Document):
    id = ObjectIdField(required=True,primary_key=True)
    traffic_type = StringField(max_length = 45)
    note = StringField(max_length = 45)
    start_time = ComplexDateTimeField()  ### '出发时间',
    arrive_time = ComplexDateTimeField()  ### '到达城市时间\n',
    total_time = StringField(max_length = 45)
    product_info_id = ReferenceField(P_Product_Info)
    traffic_id = ReferenceField(B_Traffic)
    ### '产品交通信息';
