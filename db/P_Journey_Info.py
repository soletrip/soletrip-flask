# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.F_Sub_Folder import F_Sub_Folder
from subsystem.db.U_Person_Account import U_Person_Account
from mongoengine.fields import ReferenceField, DictField
from mongoengine.fields import ComplexDateTimeField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class P_Journey_Info(Document):
    id = ObjectIdField(required=True,primary_key=True)
    user_label = IntField()  ### '用户个人标签',
    public_level = StringField(max_length = 45)  ### '该条行程公开等级\n（1）所有公开\n（2）公司之间公开\n（3）公司内部公开\n（4）私人\n（5）……',
    create_time = ComplexDateTimeField()
    modify_time = ComplexDateTimeField()
    journey_day = IntField()  ### '行程总天数',
    travel_day = StringField(max_length = 45)  ### '游玩总天数',
    travel_night = StringField(max_length = 45)  ### '游玩总晚数',
    introduction = StringField(max_length = 45)  ### '线路基础介绍',
    photo = StringField(max_length = 45)
    hot = IntField()
    person_account_id = ReferenceField(U_Person_Account)
    sub_folder_id = ReferenceField(F_Sub_Folder)
    labels = DictField(max_length = 45)  ### '标签名称':'标签描述'
    ##description = StringField(max_length = 45)  ### '标签描述',
    ### '一条行程的基础信息';
