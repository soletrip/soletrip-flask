# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from mongoengine.fields import IntField


class B_SH_Distance(Document): # I think it would be phase out, because calculate may fast than query.
    distance = IntField()  ### '景点与酒店之间的距离\n（以同一城市之间计算）',
    scenics_id = IntField() ### for cache, no need reference.
    hotel_id = IntField() ### for cache, no need reference.
    ### '（1）依赖于景点以及酒店的经纬度，计算景点周围的酒店\n（2）只计算同一个城市内的景点与酒店之间';
