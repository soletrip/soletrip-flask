# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.P_Journey_Scenics import P_Journey_Scenics
from subsystem.db.P_Product_Info import P_Product_Info
from mongoengine.fields import ReferenceField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField
from subsystem.db.B_Scenics import B_Scenics


class P_Product_Ticket(Document):
    id = ObjectIdField(required=True,primary_key=True)
    ticket_num = IntField(required=True)
    product_info_id = ReferenceField(P_Product_Info)
    journey_scenics_id = ReferenceField(P_Journey_Scenics)
    scenics_ticket_id = ReferenceField(B_Scenics)
    ### '该产品包含的所有门票';
