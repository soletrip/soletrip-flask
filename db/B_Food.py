# encoding: utf-8
'''
Created on
@author: shitian
'''
from mongoengine.document import Document
from subsystem.db.B_3pty_Company import B_3pty_Company
from subsystem.db.B_City import B_City
from mongoengine.fields import ReferenceField
from mongoengine.fields import FloatField
from mongoengine.fields import StringField
from mongoengine.fields import IntField
from mongoengine.fields import ObjectIdField


class B_Food(Document):
    id = ObjectIdField(required=True,primary_key=True)
    cn_name = StringField(max_length = 45)
    en_name = StringField(max_length = 45)
    longitude = FloatField()  ### '经度',
    latitude = FloatField()  ### '纬度',
    address = StringField(max_length = 45)  ### '地址',
    cn_introduction = StringField(max_length = 45)  ### '餐厅介绍（中文）',
    en_introduction = StringField(max_length = 45)  ### '餐厅介绍（英文）',
    phonenum = StringField(max_length = 45)  ### '电话1',
    website = StringField(max_length = 45)  ### '餐厅主页',
    hot = IntField()  ### '是否热门',
    grade = IntField()  ### '餐厅星级',
    grade_desc = StringField(max_length = 45)  ### '星级描述，用于内部编辑理解',
    area = IntField()  ### '所在区域',
    area_desc = StringField(max_length = 45)  ### '区域说明',
    type = StringField(max_length = 45)  ### '餐厅类型',
    city_id = ReferenceField(B_City)
    company_id = ReferenceField(B_3pty_Company)
    ### '美食基础信息表';
